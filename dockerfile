FROM node:alpine

ARG BUILD_DATE
ARG VCS_REF

LABEL MAINTAINER="max@maxtpower.com" \
org.label-schema.schema-version="1.0" \
org.label-schema.build-date=${BUILD_DATE} \
org.label-schema.name="maxtpower/fload" \
org.label-schema.description="HTTP File Upload Service" \
org.label-schema.vcs-url="https://gitlab.com/m1xp3r/fload" \
org.label-schema.vcs-ref=${VCS_REF} \
org.label-schema.vendor="maxtpower"

WORKDIR /upload

RUN npm install --global http-server-upload

ENTRYPOINT [ "http-server-upload"]
CMD ["--port=80", "--max-file-size=100000", "--upload-dir=/upload"]

EXPOSE 80
